#!/usr/bin/env python

# Written by Francesco Palumbo
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


import os
import web
import datetime
import json

from collections import OrderedDict


prgname = 'lanmemo'
urls    = ('/', 'Index')
render  = web.template.render('templates/')
app     = web.application(urls, globals(), True)
dbpath  = 'db/' + prgname + '.sqlite'
db      = None


class DatabaseNotFound (Exception):
    pass


if os.path.isfile(dbpath):
    db = web.database(dbn='sqlite', db=dbpath)
else:
    raise DatabaseNotFound


db.query('PRAGMA foreign_keys = true;')


def getUserData(user='Public'):
    return db.query('''SELECT Data.data_id, Data.timestamp, data_text FROM Data 
                       JOIN User ON Data.user_id = User.user_id 
                       WHERE User.user_name = $uname
                       ORDER BY Data.timestamp DESC;''', vars={'uname': user})


def insertUserData(user='Public', text=''):
    res = db.query('SELECT user_name FROM User WHERE user_name = $uname', vars={'uname': user})

    if (not res):
        db.query('INSERT INTO User (user_name) VALUES ($uname);', vars={'uname': user})

    db.query('''INSERT INTO Data (data_text, user_id)
                SELECT $text, user_id 
                FROM User 
                WHERE user_name = $uname;''', vars={'text': text, 'uname': user})
    
    return db.query('''SELECT data_id, timestamp, data_text FROM Data 
                       WHERE data_id = last_insert_rowid()
                       AND data_text = $datatext;''', vars={'datatext': text})


def removeData(idlist):
    for did in idlist:
        db.query('''DELETE FROM Data 
                    WHERE data_id = $dataid;''', vars={'dataid': did})


class Index:
    def GET(self):
        return render.base(prgname)


    def POST(self):
        data = web.input(user='Public', text=None, remove_data=None)

        if data.remove_data:
           removeData(data.remove_data.split(','))
           return
            

        res = insertUserData(data.user, data.text) if data.text else getUserData(data.user)

        json_obj = json.dumps(
            [{'data_id'  : str(a.data_id),
              'date_time': a.timestamp.now().strftime('%d-%m-%Y %H:%M:%S'), 
              'user_data': a.data_text}
            for a in res])

        return json_obj


if __name__ == "__main__":
    app.run()
