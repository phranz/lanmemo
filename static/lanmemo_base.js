/*

# Written by Francesco Palumbo

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

var last_user = '';


function getXHR()
{
    return new XMLHttpRequest();
}

function setup()
{
    function disableEnter(event)
    {
        // disable enter on input
        if (event.keyCode == 13)
            return false;
    };


    document.getElementById('user').addEventListener
    (
        'keypress',
        disableEnter
    );

    document.getElementById('data_insert').addEventListener
    (
        'keypress',
        disableEnter
    );

    document.getElementById('switch_btn').addEventListener
    (
        'click', 
        function() 
        { 
            switchUserCallback
            (
                getXHR(), 
                document.getElementById('data_insert'), 
                document.getElementById('user'), 
                document.getElementById('userid'), 
                document.getElementById('results')
            ); 
        }
    );

    document.getElementById('add_btn').addEventListener
    (
        'click', 
        function()
        {
            insertDataCallback
            (
                getXHR(), 
                document.getElementById('data_insert'), 
                document.getElementById('user'), 
                document.getElementById('userid'), 
                document.getElementById('results')
            ); 
        }
    );

    document.getElementById('remove_btn').addEventListener
    (
        'click',
        function()
        {
            removeDataCallback
            (
                getXHR(),
                document.getElementById('results')
            );           
        }
    );

    document.getElementById('select_btn').addEventListener
    (
        'click',
        function()
        {
            var node_list = document.getElementById('results').querySelectorAll('[id^=chk]');

            for (var i = 0; i < node_list.length; ++i)
                node_list[i].checked = true;
        }
    );
;
}


function removeDataCallback(req, results)
{
    var selected = [];
    var node_list = results.querySelectorAll('[id^=chk]');

    for (var i = 0; i < node_list.length; ++i)
        if (node_list[i].checked)
            selected.push(node_list[i].id.substr(3));

    if (!selected)
        return;

    req.onreadystatechange = function() 
    {
        if (req.readyState == 4 && req.status == 200) 
            removeData(selected);
    }

    req.open("POST", '/', true);
    req.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
    req.send('remove_data=' + encodeURIComponent(selected));
}


function switchUserCallback(req, data_insert, user, userid, results) 
{
    var user_text = user.value && (user.value !== 'Public') ? user.value + "'s notes" : 'Public notes';
    
    user.value = user.value || 'Public';

    if (last_user === user.value)
        return;

    results.innerHTML = '';
    data_insert.value = '';

    userid.innerHTML = user_text;

    req.onreadystatechange = function() 
    {
        if (req.readyState == 4 && req.status == 200) 
            updateView(JSON.parse(req.responseText), results);
    }

    req.open("POST", '/', true);
    req.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
    req.send('user=' + encodeURIComponent(user.value));

    last_user = user.value;
}


function insertDataCallback(req, data_insert, user, userid, results) 
{
    var user_text = user.value && (user.value !== 'Public') ? user.value + "'s notes" : 'Public notes';
    
    user.value = user.value || 'Public';

    userid.innerHTML = user_text;

    req.onreadystatechange = function() 
    {
        if (req.readyState == 4 && req.status == 200) 
        {
            updateView(JSON.parse(req.responseText), results, true);
        }
    }

    req.open("POST", '/', true);
    req.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');

    req.send('user=' + user.value + '&text=' + encodeURIComponent(data_insert.value));
    
    data_insert.value = '';
}


function removeData(selected)
{
    selected.forEach
    (
        function(rowid) 
        { 
            var ediv = document.getElementById('row' + rowid);
            results.removeChild(ediv);
        }
    );
}


function updateView(records, results, single=false)
{
    var div_row = document.createElement('div');
    div_row.setAttribute('style', 'background-color: orange; font-weight: bold;');

    div_row.innerHTML = '<div style="width: 4%; float: left;">' 
                      + '<input type="checkbox" style="visibility: hidden">' 
                      + '</div>'
                      + '<div class="horizontal_spacer"></div>'
                      + '<div style="width: 18%; float: left;">'      
                      + 'date' 
                      + '</div>' 
                      + '<div class="horizontal_spacer"></div>'
                      + '<div style="max-width: 100%; text-align: center;">'      + 'note' + '</div>'
                      ;

    if (!single)
        results.appendChild(div_row);            


    records.forEach
    (
        function(record)
        {
            var div_row = document.createElement('div');

            div_row.setAttribute('class', 'interactive_row');
            div_row.setAttribute('id', 'row' + record.data_id);

            div_row.innerHTML = '<div class="item_head">' 
                              + '<input class="checkbox "type="checkbox" id="chk' + record.data_id + '">' 
                              + '</div>'
                              + '<div class="horizontal_spacer"></div>'
                              + '<div class="date">'      + record.date_time + '</div>' 
                              + '<div class="horizontal_spacer"></div>'
                              + '<div class="data">'      + record.user_data + '</div>'
                              ;

            if (single)
                results.insertBefore(div_row, results.childNodes[1]);
            else
                results.appendChild(div_row);
        }
    );
}


setup();

var c_event = new CustomEvent('click');

document.getElementById('switch_btn').dispatchEvent(c_event);
